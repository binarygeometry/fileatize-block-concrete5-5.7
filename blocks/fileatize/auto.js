/* File Links */
refreshfileLocationControls = function() {
	var fileLocation = $('#fileLocation').val();
	$('#fileLocationInternal').toggle(fileLocation == 1);
	$('#fileLocationExternal').toggle(fileLocation == 2);
}

$(document).ready(function() {
	$('#fileLocation').change(refreshfileLocationControls);
	refreshfileLocationControls();
});

/* Tabs */
var ccm_ugActiveTab = "ccm-block-one"; //Default tab

$(document).ready(function() {
    /*
     * Clicking on tabs
     */
    $("#ccm-block-tabs a").click(function() {
        $("li.ccm-nav-active").removeClass('ccm-nav-active');
        $("#" + ccm_ugActiveTab + "-tab").hide();
        ccm_ugActiveTab = $(this).attr('id');
        $(this).parent().addClass("ccm-nav-active");
        $("#" + ccm_ugActiveTab + "-tab").show();
    });

});

/* Hide Show Form Fields */

$('#fileatizeTemplate').on('change',function(){
    if($(this).val()=='product'){
        $('.product-only').show();
    } else {
        $('.product-only').hide();
    }
});

$(document).ready(function() {
    if($('#fileatizeTemplate').val()=='product'){
        $('.product-only').show();
    } else {
        $('.product-only').hide();
    }
});