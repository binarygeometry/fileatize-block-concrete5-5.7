<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>

<?php
switch ($fileatizeTemplate) {
	case 'button':
?>
<!-- Button -->
<?php
if (strpos($fileatizeSnippet, 'http') === 0) { ?>

<div class="fltz-button-wrapper">
<?php echo '<a href="'.$fileatizeSnippet.'" target="'.$target.'">';?>
	<button class="fltz-button<?php echo $shadow;?>"<?php echo $buttonStyles;?>>
		<span<?php echo $productButtonColor;?>>
			<?php echo $buttonText;?>
		</span>
		<span class="fltz-button-price"<?php echo $productPriceColor;?>>
			<?php echo $currency.$price;?>
		</span>
	</button><!-- END .fltz-product-button -->
<?php echo '</a>';?>
<?php echo $paypalLogo;?>
</div>

<?php
} else {
	echo '<div class="fltz-error">';
	echo $fileatizeSnippet;
	echo '</div>';
}
?>

<?php
		break;
	case 'product':
?>
<!-- Product -->
<?php
if (strpos($fileatizeSnippet, 'http') === 0) { ?>

<div class="fltz-product-wrapper<?php echo $shadow;?>"<?php echo $productWrapperStyles;?>>
	<div class="fltz-product-image">
		<?php if($productImage) { echo $productImage;}?>
	</div><!-- END .fltz-product-image -->
	<div class="fltz-product-information">
		<div class="fltz-product-title">
			<?php echo '<'.$productNameSize.$productNameColor.'>'.$productName.'</'.$productNameSize.'>';?>		
		</div><!-- END .fltz-product-title -->
		<div class="fltz-product-description">
			<p<?php echo $productDescriptionColor;?>>
				<?php echo nl2br($productDescription);?>
			</p>
		</div><!-- END .fltz-product-description -->
		<div class="fltz-product-price">
			<span<?php echo $productPriceColor;?>>
				<?php echo $currency.$price;?>
			</span>
		</div><!-- END .fltz-product-price -->
		<?php echo '<a href="'.$fileatizeSnippet.'" target="'.$target.'">';?>
			<button class="fltz-product-button"<?php if($productButtonBackgroundColor) { echo 'style="'.$productButtonBackgroundColor.'"';}?>>
				<span<?php echo $productButtonColor;?>>
					<?php echo $buttonText;?>
				</span>
			</button><!-- END .fltz-product-button -->
		<?php echo '</a>';?>
		<?php echo $paypalLogo;?>
	</div><!-- END .fltz-product-information -->
</div><!-- END .fltz-product-wrapper -->

<?php
} else {
	echo '<div class="fltz-error">';
	echo $fileatizeSnippet;
	echo '</div>';
}
?>

<?php
		break;
}
?>