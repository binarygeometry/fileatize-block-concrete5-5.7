<?php  
	defined('C5_EXECUTE') or die("Access Denied.");
	class FileatizeBlockController extends BlockController {
	
		protected $btTable = 'btFileatize';
		protected $btInterfaceWidth = "500";
		protected $btInterfaceHeight = "500";
		protected $btWrapperClass = 'ccm-ui';
		protected $btCacheBlockRecord = true;
		protected $btCacheBlockOutput = true;
		
		public function getBlockTypeName() {
			return t("Fileatize");
		}
				
		public function getBlockTypeDescription() {
			return t("Monetize Your Digital Files");
		}			

		public function getFile() {	
			if ($this->fileatizeInternalFile > 0) {
				return File::getByID($this->fileatizeInternalFile);
			}
			return false;
		}	
		
		public function getFileID() {
			return $this->fileatizeInternalFile;
		}

		public function add() {
			$this->set('productWrapperBackgroundColor', '#FFFFFF');
			$this->set('productWrapperBorderColor', '#DCDCDC');
			$this->set('productNameColor', '#C8C8C8');
			$this->set('productPriceColor', '#C8C8C8');			 
			$this->set('productButtonColor', '#FFFFFF');
			$this->set('productButtonBackgroundColor', '#851F83');			
			$this->set('productNameSize', 'h3');
			$this->set('buttonText', 'Buy Now');
			$this->set('maxWidth', '250');
		}		
		
		public function view() {
		
			## Set currency as symbol
			if($this->currency == 'USD') {
				$currency = '&#36;';
			}
			elseif($this->currency == 'GBP') {
				$currency = '&#163;';
			}
			elseif($this->currency == 'EUR') {
				$currency = '&#8364;';
			}
			$this->set('currency',$currency);
			
			## Get images if available
			$html = Loader::helper('html');		
			$productImage = $this->getproductImage();
			if ($productImage) {
				$this->set('productImage', $html->image($productImage->getURL()));
			}
			
			## customization options
			if($this->productWrapperBackgroundColor) {
				$productWrapperBackgroundColor = 'background-color: '.$this->productWrapperBackgroundColor.';';
			}
			
			if($this->productWrapperBorderColor) {
				$productWrapperBorderColor = 'border-color: '.$this->productWrapperBorderColor.';';
			}
			
			if($this->maxWidth) {
				$maxWidth = 'max-width: '.$this->maxWidth.'px;';
			}
			
			if($this->floatDirection) {
				$float = 'float: '.$this->floatDirection.';';
			}
									
			if($this->productWrapperBackgroundColor || $this->productWrapperBorderColor || $this->maxWidth || $this->floatDirection) {
				$productWrapperStyles = ' style="'.$productWrapperBackgroundColor.$productWrapperBorderColor.$maxWidth.$float.'"';
			}
			$this->set('productWrapperStyles',$productWrapperStyles);			
			
			if($this->productNameColor) {
				$productNameColor = ' style="color: '.$this->productNameColor.'"';
			}
			$this->set('productNameColor',$productNameColor);
			
			if($this->productDescriptionColor) {
				$productDescriptionColor = ' style="color: '.$this->productDescriptionColor.'"';
			}
			$this->set('productDescriptionColor',$productDescriptionColor);

			if($this->productPriceColor) {
				$productPriceColor = ' style="color: '.$this->productPriceColor.'"';
			}
			$this->set('productPriceColor',$productPriceColor);
			
			if($this->productButtonColor) {
				$productButtonColor = ' style="color: '.$this->productButtonColor.'"';
			}
			$this->set('productButtonColor',$productButtonColor);
			
			if($this->productButtonBackgroundColor) {
				$productButtonBackgroundColor = 'background-color: '.$this->productButtonBackgroundColor.';';
			}
			$this->set('productButtonBackgroundColor',$productButtonBackgroundColor);
			
			if($this->productButtonBackgroundColor || $this->maxWidth || $this->floatDirection) {
				$buttonStyles = ' style="'.$productButtonBackgroundColor.$maxWidth.$float.'"';
			}
			$this->set('buttonStyles',$buttonStyles);			
			
			if($this->shadow) {
				$shadow = ' fltz-shadow';
			}
			$this->set('shadow',$shadow);

			if($this->paypalLogo) {
				$paypalLogo = '<a class="fltz-paypal-logo" href="https://www.paypal.com/uk/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open("https://www.paypal.com/uk/webapps/mpp/paypal-popup","WIPaypal","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700"); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-100px.png" border="0" alt="PayPal Logo"></a>';
			}
			$this->set('paypalLogo',$paypalLogo);			
			
											
		}

		public function getproductImage() {
			if ($this->productImage > 0) {
				return File::getByID($this->productImage);	
			}
			return false;	
		}
		
		public function validate($args) {
			$e = Loader::helper('validation/error');
			
			if (empty($args['productName'])) {
				$e->add('A product name is required.');
			}
			if (empty($args['currency'])) {
				$e->add('A currency is required.');
			}
			if (empty($args['price'])) {
				$e->add('A price is required.');
			}
			
			if (!is_numeric($args['price'])) {
				$e->add('Price must be a number.');
			}

			if(!filter_var($args['emailAddress'], FILTER_VALIDATE_EMAIL)) {
				$e->add('A Valid PayPal email address is required.');
    		}
    		
			if (empty($args['fileatizeInternalFile']) && empty($args['fileatizeExternalFile'])) {
				$e->add('A file is required.');
    		}
    		
			return $e;
		}
		
		public function save($args) {
				
			## Create button here and store. Will be faster, and reduce load on API
			Loader::library('fileatize', 'fileatize');
			$f = new fileatize();

			## Setup  product
			$f->setName($args['productName']);
			$f->setPrice($args['price']);
			$f->setCurrency($args['currency']);
			$f->setEmail($args['emailAddress']);

			## File location
			if($args['fileatizeExternalFile'] != '') {
				$fileURI = $args['fileatizeExternalFile'];
			} else {
				$fv = File::getByID($args['fileatizeInternalFile']);
				$fileURI = $fv->getUrl();
			}	

			$f->setDownloadURI($fileURI);

			## Get link (needs to be a live site so the API can reach the file)
			$args['fileatizeSnippet'] = $f->getLink();
			
			$args['enableButton'] = isset($args['enableButton']) ? 1 : 0;

			parent::save($args);
		}	
	}
?>