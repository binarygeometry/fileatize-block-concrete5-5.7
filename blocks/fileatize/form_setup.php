<?php  
defined('C5_EXECUTE') or die(_("Access Denied.")); 
$form = Loader::helper('form');
$fh = Loader::helper('form/color');
$al = Loader::helper('concrete/asset_library');
?>

<style>
	.ccm-file-manager-select {
		width: 310px;
	}
	div.ccm-file-selected-wrapper {
		width: 287px;
	}
</style>

<ul class="ccm-dialog-tabs tabs" id="ccm-block-tabs">
	<li class="ccm-nav-active">
		<a href="javascript:void(0)" id="ccm-block-one"><?php  echo t('Product Information') ?></a>
	</li>
	<li>
		<a href="javascript:void(0)" id="ccm-block-two"><?php  echo t('Options') ?></a>
	</li>
</ul>
<div id="tabs">
	<div id="ccm-block-one-tab">

		<div class="ccm-block-field-group">
		
			<div class="clearfix">
				<label>
					<?php echo t('Template');?>
				</label>
				<div class="input">
					<?php echo $form->select('fileatizeTemplate', array('button'=>t('Button'), 'product'=>t('Product')), $fileatizeTemplate, array('style'=>'width:310px;'));?>
					<span class="help-block"><?php echo t('Choose a template');?></span>
				</div>
			</div><!-- END .clearfix -->		

		</div><!-- END .ccm-block-field-group -->
		
		<div class="ccm-block-field-group">

			<div class="clearfix">
				<label>	
				<?php echo t('Product Name');?>		
				</label>
				<div class="input">
					<?php echo $form->text('productName', $productName, array('style'=>'width:240px;'));?>
					<?php echo $form->select('productNameSize', array('h1'=>t('H1'), 'h2'=>t('H2'), 'h3'=>t('H3'), 'h4'=>t('H4'), 'h5'=>t('H5'), 'h6'=>t('H6')), $productNameSize, array('style'=>'width:60px;'));?>
					<span class="help-block"><?php echo t('Enter your product name - Required');?></span>
				</div>
			</div><!-- END .clearfix -->
			
			<div class="clearfix product-only" style="display:none;">
				<label>	
				<?php echo t('Product Description');?>		
				</label>
				<div class="input">
					<?php echo $form->textarea('productDescription', $productDescription, array('style'=>'width:300px;'));?>
					<span class="help-block"><?php echo t('Enter your product description - Optional');?></span>
				</div>
			</div><!-- END .clearfix -->
			
			<div class="clearfix product-only" style="display:none;">
				<label>	
				<?php echo t('Product Thumbnail');?>		
				</label>
				<div class="input">
					<?php echo $al->image('ccm-b-image', 'productImage', t('Choose Image'), $this->controller->getproductImage()); ?>
					<span class="help-block"><?php echo t('Choose your thumbnail - Optional');?></span>
				</div>
			</div><!-- END .clearfix -->
		
			<div class="clearfix">
				<label>	
				<?php echo t('Price');?>		
				</label>
				<div class="input">
					<?php echo $form->select('currency', array('USD'=>t('&#36; (Dollar)'), 'GBP'=>t('&#163; (Pound)'), 'EUR'=>t('&#8364; (Euro)')), $currency, array('style'=>'width:110px;'));?>
					<?php echo $form->text('price', $price, array('style'=>'width:185px;'));?>
					<span class="help-block"><?php echo t('Enter your price - Required');?></span>
				</div>
			</div><!-- END .clearfix -->
			
			<div class="clearfix">
				<label>	
				<?php echo t('Email Address');?>		
				</label>
				<div class="input">
					<?php echo $form->text('emailAddress', $emailAddress, array('style'=>'width:300px;'));?>
					<span class="help-block"><?php echo t('Enter your PayPal email address - Required');?></span>
				</div>
			</div><!-- END .clearfix -->
		
			<div class="clearfix">
				<label>	
				<?php echo t('File');?>		
				</label>		
				<div class="input">	
					<select name="fileLocation" id="fileLocation" style="width:310px;">
						<option value="1" <?php echo (empty($fileatizeExternalFile) && !empty($fileatizeInternalFile) ? 'selected="selected"' : '')?>><?php echo t('File Manager')?></option>
						<option value="2" <?php echo (!empty($fileatizeExternalFile) ? 'selected="selected"' : '')?>><?php echo t('External URL')?></option>
					</select>
				</div>
			</div><!-- END .clearfix -->
		
			<div id="fileLocationInternal" style="display: none;" class="clearfix">
				<div class="clearfix">
					<div class="input">
						<?php echo $al->file('ccm-b-file', 'fileatizeInternalFile', t('Choose File'), $this->controller->getFile()); ?>			
						<span class="help-block"><?php echo t('Choose your file - Required');?></span>
					</div>
				</div><!-- END .clearfix -->
			</div>
			<div id="fileLocationExternal" style="display: none;" class="clearfix">
				<div class="clearfix">
					<div class="input">
						<?php echo $form->text('fileatizeExternalFile', $fileatizeExternalFile, array('style'=>'width:300px;'));?>
						<span class="help-block"><?php echo t('Enter an external URL for your file - Required');?></span>
					</div>
				</div><!-- END .clearfix -->
			</div>

		</div><!-- END .ccm-block-field-group -->
		
	</div><!-- END #ccm-block-one-tab -->
	
	<div id="ccm-block-two-tab" style="display: none">

		<div class="ccm-block-field-group">

			<div class="clearfix">
				<label>
					<?php echo t('Float');?>
				</label>
				<div class="input">
					<?php echo $form->select('floatDirection', array('none'=>t('None'), 'left'=>t('Left'), 'right'=>t('Right')), $floatDirection, array('style'=>'width:310px;'));?>
					<span class="help-block"><?php echo t('Choose a float');?></span>
				</div>
			</div><!-- END .clearfix -->		
		
			<div class="clearfix">
				<label>
					<?php echo t('Target');?>
				</label>
				<div class="input">
					<?php echo $form->select('target', array('_blank'=>t('New Window'), '_self'=>t('Same Window')), $target, array('style'=>'width:310px;'));?>
					<span class="help-block"><?php echo t('Choose how your link opens');?></span>
				</div>
			</div><!-- END .clearfix -->			

			<div class="clearfix">
				<label>	
				<?php echo t('Button Text');?>		
				</label>
				<div class="input">
					<?php echo $form->text('buttonText', $buttonText, array('style'=>'width:300px;'));?>
					<span class="help-block"><?php echo t('Enter your button text');?></span>
				</div>
			</div><!-- END .clearfix -->

			<div class="clearfix">
				<label>	
				<?php echo t('Max Width');?>		
				</label>
				<div class="input">
					<?php echo $form->text('maxWidth', $maxWidth, array('style'=>'width:300px;'));?>
					<span class="help-block"><?php echo t('Enter a maximum width');?></span>
				</div>
			</div><!-- END .clearfix -->			
			
		</div><!-- END .ccm-block-field-group -->
		
		<div class="ccm-block-field-group">
				
			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Product Background Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productWrapperBackgroundColor', '', $productWrapperBackgroundColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->

			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Product Border Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productWrapperBorderColor', '', $productWrapperBorderColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->
	
			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Product Name Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productNameColor', '', $productNameColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->

			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Product Description Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productDescriptionColor', '', $productDescriptionColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->
			
			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Product Price Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productPriceColor', '', $productPriceColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->

			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Button Text Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productButtonColor', '', $productButtonColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->

			<div class="clearfix">
				<label style="text-align:left;">	
				<?php echo t('Button Background Color');?>		
				</label>
				<div class="input">
					<?php echo $fh->output('productButtonBackgroundColor', '', $productButtonBackgroundColor, array('style'=>'width:300px;'));?>
				</div>
			</div><!-- END .clearfix -->
						
			<div class="clearfix">
				<div class="input">
					<?php echo $form->checkbox('shadow', 1, $shadow);?> <?php echo t('Enable shadow');?>
				</div>
			</div><!-- END .clearfix -->

			<div class="clearfix">
				<div class="input">
					<?php echo $form->checkbox('paypalLogo', 1, $paypalLogo);?> <?php echo t('Enable PayPal logo');?>
				</div>
			</div><!-- END .clearfix -->

		</div><!-- END .ccm-block-field-group -->

	</div><!-- END #ccm-block-two-tab -->
	
</div><!-- END #tabs -->